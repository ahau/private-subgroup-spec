# Group application

## Approved path

```mermaid
sequenceDiagram
  autonumber
  
  rect rgba(177,0,40,0.22)
    applicant->>kaitiaki: apply to join
      Note over kaitiaki: profile/person/admin
      Note over kaitiaki: application
  end
  
  rect rgba(177,0,40,0.22)
    kaitiaki->>group: add applicant
      Note over group: profile/person
      Note over group: link/feed-profile
      Note over group: group/add-member
      
      
    kaitiaki->>kaitiaki: update application
      Note over kaitiaki: application
  end
   
  kaitiaki-->>applicant: replication
```

## Declined path

```mermaid
sequenceDiagram
  autonumber
  
  rect rgba(177,0,40,0.22)
    applicant->>kaitiaki: apply to join
      Note over kaitiaki: profile/person/admin
      Note over kaitiaki: application
  end
  
  rect rgba(177,0,40,0.22)
    kaitiaki->>kaitiaki: update application
      Note over kaitiaki: application
      
    kaitiaki->>kaitiaki: tombstone profile
      Note over kaitiaki: profile/person/admin
  end
   
  kaitiaki-->>applicant: replication
  applicant-->>applicant: unboxes decline
```
