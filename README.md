# private-subgroup-spec | v1.0.0

A specification for implementing private subgroups in scuttlebutt.

The fundamentals of this spec are:

* a subgroup MUST have its own **groupKey** used to encrypt records to the subgroup
* a subgroup MUST have its own **groupId** which uniquely identifies it
    * defined as the the "cloaked message id" for the `group/init` message of the subgroup
* a subgroup MUST be linked to a **parent group**
    ```js
    {
      type: 'link/group-group/subgroup',
      parent: parentGroupId,
      child: subGroupId,
      recps: [parentGroupId] // NOTE if you want to hide from the parent group, encrypt to subGroupId
    }
    ```
* **members** of a subgroup:
    * the creator of the subgroup, and all those invited to join
    * MUST be members of the parent group as well
    * authorised to decrypt records encrypted to the subgroup

* **kaitiaki** of a subgroup (those who _look after_ it) are: (MOVE THIS LATER)
    * the **creator** of the subgroup
    * current authors of the subgroups profile

Optional:
* (optional) a subgroup CAN have a direct-messaging key (called `poBoxId`) - used by non-members to send something to the subgroup



## Minimal Subgroup setup

```mermaid
graph TB

subgraph group
  groupId1((group ID))
  groupKey1[group key]
  groupId1((group ID))
  init1[group/init]
  
  linkGroupSubgroup([link/group-group/subgroup])
  
  groupKey1-..->init1-..->groupId1
end

subgraph sub-group
  groupKey2[group key]
  groupId2((group ID))
  init2[group/init]
  poBoxId
  
  groupKey2-..->init2-..->groupId2
end

groupId1-->linkGroupSubgroup-->groupId2


classDef default fill:purple, stroke:purple, stroke-width 1, color: white;
classDef cluster fill:#ff00ff22, stroke:purple, border-radius: 20px;

classDef off-chain fill:gainsboro, stroke:black, stroke-dasharray: 2;
class groupKey1,groupId1 off-chain
class groupKey2,groupId2,poBoxId off-chain

classDef subgroup-msg fill:#000099, stroke:purple, stroke-width 1, color: white;
class init2 subgroup-msg
```

1. a  `group` is made with its own **group key**
2. a `subgroup` is made with its own **group key** and **direct message key** (`poBoxId`)
3. the `subgroup` is linked to the `group` using a `link/group-group/subgroup` message

In this setup it's up to you how to distribute / advertise the `poBoxId`

### Advanced Example

This is an example from Āhau's setup, which uses additional messages:
- `profile/community` records (messages) for describing groups (including advertising the `poBoxId`)
- `group/po-box` messages distributing the keys to a P.O. Box withing a (sub)group

```mermaid
graph TB

subgraph sub-group
  groupKey2[group key]
  groupId2((group ID))
  init2[group/init]
  poBoxId2[group/po-box]
  
  community2[profile/community]
  link2A([link/group-profile])
  
  init2-->poBoxId2
  groupKey2-..->init2-..->groupId2
  groupId2-->link2A-->community2
end


subgraph group
groupId1((group ID))
  groupKey1[group key]
  groupId1((group ID))
  init1[group/init]
  
  community1[profile/community]
  link1([link/group-profile])
  
  communitySubgroup[profile/community]
  linkGroupSubgroup([link/group-group/subgroup])
  linkSubgroupProfile([link/group-profile])
  
  groupKey1-..->init1-..->groupId1
  groupId1-->link1-->community1
  groupId1-->linkGroupSubgroup-->groupId2
  groupId2-->linkSubgroupProfile-->communitySubgroup
end



classDef default fill:purple, stroke:purple, stroke-width 1, color: white;
classDef cluster fill:#ff00ff22, stroke:purple, border-radius: 20px;

classDef off-chain fill:gainsboro, stroke:black, stroke-dasharray: 2;
class groupKey1,groupId1 off-chain
class groupKey2,groupId2 off-chain

classDef subgroup-msg fill:#000099, stroke:purple, stroke-width 1, color: white;
class init2,poBoxId2,community2,link2A,communitySubgroup,linkSubgroupProfile subgroup-msg
```

NOTES:
- there is a copy of the subgroup profile published to (i.e. encrypted to) the parent group
    - this is so people in the parent group can see a subgroup profile, and perhaps apply to join
    - these are coloured blue just to make it clear that the content is from the subgroup



## Diagram Key

```mermaid
graph LR

subgraph Key
  key(( group ID )) -..- keyi[off-chain data, here a unique ID for the group]
  Init[ group/init ] -..- Initi[encrypted msg, of type `group/init`]
  parent --> Link([link/parent-child]) --> child
  
  Link -..- Linki[encrypted `link` msg pointing from parent > child]
  
  
  Public[ profile/communiy ] -..- Publici[public msg of type `profile/community`]
end


classDef default fill:none, stroke: none;

classDef info fill:none, stroke:black;
classDef private-msg fill:purple, stroke:none, color: white;
classDef public-msg fill:white, stroke:purple, stroke-width: 1, color: black;
classDef off-chain fill:gainsboro, stroke:black, stroke-width: 1, stroke-dasharray: 2;

class Key info
class Init,Link private-msg
class Public public-msg
class key off-chain
```

