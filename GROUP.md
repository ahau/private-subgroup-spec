# Groups

The following is an example of how a group can be set up (with public and group profiles)

```mermaid
graph TB

community0[profile/community]
link0A([link/group-profile])
groupId1-->link0A-->community0

subgraph group
  groupId1((group ID))
  community1[profile/community]
  link1A([link/group-profile])
  
  groupId1-->link1A-->community1
end


classDef default fill:purple, stroke:purple, stroke-width 1, color: white;
classDef cluster fill:#ff00ff22, stroke:purple;

classDef public-msg fill:white, stroke:purple, stroke-width: 1, color: black;
classDef off-chain fill:gainsboro, stroke:black, stroke-dasharray: 2;

class community0,link0A public-msg;
class key1,key2,groupId1,groupId2 off-chain
```
